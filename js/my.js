var kanaJSON,
    stopTimeInSeconds,
    hiddenField;

function init() {
    document.getElementById("stopBtn").disabled = true;
}
function start(kana) {
    loadKana(kana);
    document.getElementById("generator-type").innerHTML = kana;
    hiddenField = document.getElementById("secondField").lastElementChild.firstElementChild;
    hiddenField.style.opacity = "0.0";
    setTimeout(updateTimer, 1000);
    document.getElementById("fieldSwitcher").disabled = true;
    document.getElementById("startBtn").disabled = true;
    document.getElementById("stopBtn").disabled = false;
}

function stop() {
    stopTimeInSeconds = Math.floor(Date.now() / 1000);
    document.getElementById("timer").innerHTML = " in 0:00";
    hiddenField.style.opacity = "1.0";
    document.getElementById("fieldSwitcher").disabled = false;
    document.getElementById("startBtn").disabled = false;
    document.getElementById("stopBtn").disabled = true;
}

function loadKana(kana) {
    var file = "js/" + kana + ".json",
            xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            kanaJSON = JSON.parse(this.responseText);
            generate();
        }
    };

    xhttp.open("GET", file, true);
    xhttp.send();
}

function controlValues() {
    var size = Math.max(0, document.getElementById("size").value),
        minutes = Math.max(0, document.getElementById("minutes").value),
        seconds = Math.max(0, document.getElementById("seconds").value);

    document.getElementById("size").value = size;
    document.getElementById("minutes").value = minutes;
    document.getElementById("seconds").value = seconds;
    
    stopTimeInSeconds = Math.ceil(Date.now() / 1000) + minutes * 60 + seconds;
}

function generate() {
    var size = document.getElementById("size").value,
        wordSize = document.getElementById("wordSize").value,
        kana = document.getElementById("kana"),
        romaji = document.getElementById("romaji"),
        section, key, value,
        kanas = [],
        nextSpace = Math.floor(Math.random() * wordSize) % wordSize;
    // Reset old values
    controlValues();
    kana.innerHTML = "";
    romaji.innerHTML = "";
    // Create array containing all available kana/romaji pairs
    for (section in kanaJSON) {
        if (!kanaJSON.hasOwnProperty(section)) {
            continue;
        } else {
            for (key in kanaJSON[section]) {
                if (!kanaJSON[section].hasOwnProperty(key)) {
                    continue;
                } else {
                    // A little bit card mixing technique
                    if (kanas.length % 2 === 0) {
                        kanas.push([key, kanaJSON[section][key]]);
                    } else {
                        kanas.unshift([key, kanaJSON[section][key]]);
                    }
                }
            }
        }
    }
    // Generate random sequence
    for (var i = 0; i < size; ++i) {
        var index = Math.floor(Math.random() * kanas.length) % kanas.length;
        
        kana.innerHTML += kanas[index][0];
        romaji.innerHTML += kanas[index][1];
        
        if (nextSpace <= 0) {
            kana.innerHTML += " ";
            romaji.innerHTML += " ";
            nextSpace = Math.floor(Math.random() * wordSize) % wordSize;
        }
        nextSpace--;
    }
}

function updateTimer() {
    var nowSeconds = Math.floor(Date.now() / 1000),
        difference = stopTimeInSeconds - nowSeconds,
        minutes = Math.floor(difference / 60),
        seconds = difference % 60,
        secondsAsString = seconds < 10 ? "0" + seconds : seconds;

    document.getElementById("timer").innerHTML = " in " + minutes + ":" + secondsAsString;
    
    if (nowSeconds < stopTimeInSeconds) {
        setTimeout(updateTimer, 1000);
    } else {
        stop();
    }
}

function exchangeFields() {
    var firstField = document.getElementById("firstField"),
        secondField = document.getElementById("secondField"),
        temp;
    
    temp = firstField.innerHTML;
    firstField.innerHTML = secondField.innerHTML;
    secondField.innerHTML = temp;
    hiddenField = secondField.lastElementChild.firstElementChild;
}