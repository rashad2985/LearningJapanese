# LearningJapanese

This is a little tool for training Hiragana and Katakana that generates random sequence of syllables in romaji/kana and vice versa.

User chooses hiragana or katakana to train. Than sets number of syllables and time to translate to/from kana from/to romaji.

Using this method either kana writing skills or reading skills can be trained.

Attention: Generated syllables forms a random sequence. The meaning of generated sequences should not be considered as words or sentences in japanese language. They are not.
